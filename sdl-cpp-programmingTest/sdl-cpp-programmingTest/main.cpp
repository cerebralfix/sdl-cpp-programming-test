#include <SDL.h>
#include <stdio.h>

const int SCREEN_HEIGHT = 500;
const int SCREEN_WIDTH = 750;

int main( int argc, char* args[] )
{
	SDL_Window* gameWindow = NULL;
	
	SDL_Surface* screenSurface = NULL;

	// Error check that SDL is initialised correctly
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialise! SDL_Error: %s\n", SDL_GetError() );
	}
	else
	{
		// Create the game window
		gameWindow = SDL_CreateWindow( "sdl c++ programming test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if(gameWindow == NULL )
		{
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
		}
		else
		{
			//Get window surface
			screenSurface = SDL_GetWindowSurface(gameWindow);

			//Fill the surface white
			SDL_FillRect( screenSurface, NULL, SDL_MapRGB( screenSurface->format, 0xFF, 0xFF, 0xFF ) );
			
			//Update the surface
			SDL_UpdateWindowSurface( gameWindow );

			//Wait two seconds
			SDL_Delay( 2000 );
		}
	}

	return 0;
}